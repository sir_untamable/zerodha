#!/bin/sh

celery multi start worker1 -A data_scrapper
celery flower -A data_scrapper --address=0.0.0.0 --port=5555&
python3 run_server.py


import cherrypy
import os
from web.index import HelloWorld


current_dir = os.path.dirname(os.path.abspath(__file__))
static_dir = os.path.join(current_dir, 'web', 'static')
config = { '/': {'tools.staticdir.root': static_dir},
           '/static': {'tools.gzip.on': True,
                       'tools.staticdir.on': True,
                       'tools.staticdir.dir': ''},
           '/static/css': {'tools.gzip.mime_types':['text/css'],
                           'tools.staticdir.dir': 'css'},
           '/static/js': {'tools.gzip.mime_types': ['application/javascript'],
                          'tools.staticdir.dir': 'js'},
           }



if __name__ == '__main__':
    cherrypy.config.update({'server.socket_port': 80,'server.socket_host':'0.0.0.0'})
    cherrypy.quickstart(HelloWorld(),'/',config)
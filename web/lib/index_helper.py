from .redis import redis_cache


def get_top_10_stocks():
    stocks = []
    for stock in redis_cache.sort('equity',start = 0,num = 10,by='equity*->SC_CODE'):
        stocks.append(redis_cache.hgetall('equity'+stock))
    return stocks


def search_stock(stock_name):
    stocks = []
    for stock in redis_cache.sscan_iter('equity',match=stock_name+"*"):
        stocks.append(redis_cache.hgetall('equity'+stock))
    return stocks
import requests
from bs4 import BeautifulSoup
from io import BytesIO,TextIOWrapper
import zipfile
import csv
from data_scrapper.data_helper import insert_or_update_row,get_collection_members,delete_rows



def scrap_url(url,id):
    r = requests.get(url)
    data = r.text
    soup = BeautifulSoup(data, "html.parser")
    link_to_download = soup.find(id=id)['href']
    return link_to_download


def get_csv_file(link_to_download,csv_file_name = None):
    downloaded_file =  requests.get(link_to_download)
    zip_file = zipfile.ZipFile(BytesIO(downloaded_file.content))
    
    if csv_file_name:
        csv_file = zip_file.open(csv_file_name)
    else:
        csv_file = zip_file.open(zip_file.namelist()[0])
    csv_file = TextIOWrapper(csv_file, encoding='iso-8859-1', newline='')
    return csv.DictReader(csv_file, delimiter = ',')

def delete_old_stocks(collection,stocks):
    stocks_in_redis = get_collection_members(collection)
    stocks_to_delete = [i for i in stocks_in_redis if i not in stocks]
    delete_rows(collection,stocks_to_delete)


def insert_stocks(collection,stocks_to_insert):
    stocks = list(map(lambda stock: stock['SC_NAME'],stocks_to_insert))
    delete_old_stocks(collection,stocks)

    for stock in stocks_to_insert:
        insert_or_update_row(collection,stock['SC_NAME'],stock)


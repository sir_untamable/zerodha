import redis
import os
from web.settings import *

redis_cache = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0, charset="utf-8",decode_responses=True)

import cherrypy
from web.lib.common import render_template
from web.lib.index_helper import get_top_10_stocks,search_stock


class HelloWorld(object):
    @cherrypy.expose
    def index(self,q = ''):
        stocks = []
        if len(q)>0:
            print(type(q))
            stocks = search_stock(q.upper())
            print(stocks)
        else:
            stocks = get_top_10_stocks()
        return render_template('index.html',stocks = stocks,q=q)

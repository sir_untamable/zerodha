from .task_helper import get_csv_file,scrap_url,insert_stocks
from .celery import app

@app.task
def add_two():
    return 1+2


@app.task
def get_bhav_copy():
    link_to_download = scrap_url('http://www.bseindia.com/markets/equity/EQReports/Equitydebcopy.aspx','btnhylZip')
    csv_reader = get_csv_file(link_to_download)
    
    stocks_to_insert = []
    for line in csv_reader:
        data = {}
        data['SC_NAME'] = line['SC_NAME']
        data['SC_CODE'] = line['SC_CODE']
        data['OPEN'] = line['OPEN']
        data['HIGH'] = line['HIGH']
        data['LOW'] = line['LOW']
        data['CLOSE'] = line['CLOSE']
        stocks_to_insert.append(data)
        
    count = insert_stocks('equity',stocks_to_insert)
    
    return count
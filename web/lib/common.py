from web.templates import jinja_env


def render_template(template, **kwargs):
    
    return jinja_env.get_template(template).render(**kwargs)

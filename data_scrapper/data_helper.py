from web.lib.redis import redis_cache


def insert_or_update_row(collection,key,value):
    redis_cache.hmset(collection+str(key),value)
    redis_cache.sadd(collection,str(key))


def delete_row(collection,key):
    redis_cache.delete(collection+str(key))
    redis_cache.srem(collection,str(key))


def get_collection_members(collection):
    return redis_cache.smembers(collection)


def delete_rows(collection,rows):
    for row in rows:
        delete_row(collection,row)

# Zerodha Equity index
The application consists of two components
 - A CherryPy web application (web package)
 - A Celery background job (data_scrapper package)

### Web Application
To start the web application
 - Install the dependencies in requirements.txt
 - Using python run python run_server.py

### Scheduling the task
From the python shell execute this-
```python
from data_scrapper.tasks import get_bhav_copy
get_bhav_copy.delay()
```
This will fetch and the CSV file and load it into redis, taking care of deletion of old records(if present)

#### Deployment method used
 - The Application is deployed using docker enviroment
 - After installing the required docker dependencies do
```sh
$ docker-compose build
$ docker-compose up -d
```
from __future__ import absolute_import

from celery import Celery
from web.settings import *


app = Celery("data_scrapper", broker="redis://"+REDIS_HOST, include=["data_scrapper.tasks"])

# Load configuration from the configuration object
app.config_from_object("data_scrapper.celeryconfig")

# Update the configuration
app.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

